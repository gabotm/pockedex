//
//  Pokemon.swift
//  Pockedex
//
//  Created by Gabriel Torres on 2/06/22.
//

import Foundation
import SwiftUI


struct Pokemon {
    var id: Int
    var nombre: String
    var imagen: Image

}
