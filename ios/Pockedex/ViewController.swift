//
//  ViewController.swift
//  Pockedex
//
//  Created by Gabriel Torres on 2/06/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
        
    // MARK: declaraciones
    
    @IBOutlet weak var txtBuscador: UITextField!
    
    @IBOutlet weak var tbvListado: UITableView!
    var filtrado: Bool = false
    var listaPokemon = Array<Array<String>>()
    var pokemonFiltrados = Array<Array<String>>()
    
    // MARK: lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        obtenerPokemon()
        
        tbvListado.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tbvListado.delegate = self
        tbvListado.dataSource = self
        tbvListado.rowHeight = 70
        
        txtBuscador.addTarget(self, action: #selector(self.self.textFieldDidChange(_:)),
        for: .editingChanged)
        

    }
    
    
    // MARK: metodos de usuario y delegados
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtrado == true ? pokemonFiltrados.count : listaPokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let pokemonActual = filtrado == true ? pokemonFiltrados[indexPath.row] : listaPokemon[indexPath.row]
        var cell:UITableViewCell? =
            tableView.dequeueReusableCell(withIdentifier: "cell")

            cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle,
                        reuseIdentifier: "cell")
        
        cell?.textLabel?.text = "\(pokemonActual[1].capitalized)"
        cell?.detailTextLabel?.text = "#:\(pokemonActual[0].components(separatedBy: "/")[6])"
        cell?.detailTextLabel?.numberOfLines=2
        cell?.textLabel?.font=UIFont.systemFont(ofSize: 13.0, weight: UIFont.Weight.bold)
        cell?.detailTextLabel?.font=UIFont.systemFont(ofSize: 12.0)
        
        return (cell ?? nil)!
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let searchText = textField.text!
        if (searchText.count>0) {
            filtrado = true
            
            pokemonFiltrados = listaPokemon.filter {
                $0[1].localizedCaseInsensitiveContains(searchText)
            }

        }
        else
        {
            filtrado = false
            pokemonFiltrados = listaPokemon
        }
        self.tbvListado.reloadData()
        
    }
    
    /*
        Descripcion: Metodo para consumir endpoint listado pokemon servicio REST  https://pokeapi.co/api/v2/pokemon.
        Parametros:  ?
        Retorna: vacio (void)
     */
    func obtenerPokemon() {
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let request : NSMutableURLRequest = NSMutableURLRequest()
        
        request.url = NSURL(string: "https://pokeapi.co/api/v2/pokemon") as URL?
        request.httpMethod="GET"
        
        let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in

            
            if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                let datosJson = NSString(data: data, encoding: String.Encoding.utf8.rawValue)!

                do {
                    let dataPokemon = try JSONSerialization.jsonObject(with: datosJson.data(using: String.Encoding.utf8.rawValue)!, options : .allowFragments) as? [String:Any]
                    
                    if dataPokemon != nil {
                        let lista = dataPokemon!["results"]
                        for p in lista as! [AnyObject] {
                            var pokemon = Array<String>()
                            pokemon.append("\(p["url"]! ?? "")") // id
                            pokemon.append("\(p["name"]! ?? "")") // nombre
                            self.listaPokemon.append(pokemon)

                        }
                    }
                } catch {
                    print(error.localizedDescription)
                    print("Error al obtener información")
                }
                
            }
            

        }
        task.resume()
    }


}

