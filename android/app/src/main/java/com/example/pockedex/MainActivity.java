package com.example.pockedex;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    EditText txtBuscador;
    ListView lsvListado;

    ListViewAdapter adaptador;
    ArrayList<Pokemon> datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        datos = new ArrayList<Pokemon>();

//        for (int i=0; i<3; i++) {
//            Pokemon p = new Pokemon();
//            p.setName("pokemon " + i);
//            p.id = i+1;
//            datos.add(p);
//        }

        obtenerPokemons();

        txtBuscador = findViewById(R.id.txtBuscador);
        lsvListado = findViewById(R.id.lsvListado);

        Log.i("Lista Pokemon ==>", String.valueOf(datos.size()));

//        adaptador = new ListViewAdapter(this, datos);

        lsvListado.setTextFilterEnabled(true);
//        lsvListado.setAdapter(adaptador);

        txtBuscador.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                adaptador.filter(arg0.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });


    }

    private void obtenerPokemons() {
        Call<PokemonResult> call = RetrofitClient.getInstance().getMyApi().obtenerPokemon();
        call.enqueue(new Callback<PokemonResult>() {
            @Override
            public void onResponse(Call<PokemonResult> call, Response<PokemonResult> response) {
                PokemonResult pokemonResult = response.body();
                List<Pokemon> listaPokemon = pokemonResult.results;

                for (int i = 0; i < listaPokemon.size(); i++) {
                    Pokemon p = new Pokemon();
                    p.setName(listaPokemon.get(i).getName());
                    p.id = i+1;
                    datos.add(p);
//                    Log.i("Pokemon",  listaPokemon.get(i).getName());
                }

                adaptador = new ListViewAdapter(MainActivity.this, datos);
                lsvListado.setAdapter(adaptador);

                Log.i("Exito",  "Información obtenida correctamente");
                Log.i("Lista Pokemon", String.valueOf(listaPokemon.size()));
            }

            @Override
            public void onFailure(Call<PokemonResult> call, Throwable t) {
                Log.i("Error",  t.toString());
                Toast.makeText(getApplicationContext(), "Error al obtener la información ", Toast.LENGTH_LONG).show();
            }

        });
    }
}