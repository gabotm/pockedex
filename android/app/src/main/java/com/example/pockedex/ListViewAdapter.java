package com.example.pockedex;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

public class ListViewAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Pokemon> listaPokemon = new ArrayList<>();
    private ArrayList<Pokemon> listaFiltro = new ArrayList<>() ;

    public ListViewAdapter(Context context, ArrayList<Pokemon> listaPokemon) {
        mContext = context;
        listaPokemon = listaPokemon;
        listaFiltro = listaPokemon;
        inflater = LayoutInflater.from(mContext);
        Log.i("Lista Pokemon", String.valueOf(listaPokemon.size()));
    }

    public class ViewHolder {
        TextView Titulo;
        TextView Subtitulo;
    }

    @Override
    public int getCount() {
        return listaFiltro != null ? listaFiltro.size() : 0;
    }

    @Override
    public Pokemon getItem(int position) {
        return listaFiltro.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listitem, null);

            holder.Titulo = (TextView) view.findViewById(R.id.LblTitulo);
            holder.Subtitulo = (TextView) view.findViewById(R.id.LblSubTitulo);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.Titulo.setText(listaFiltro.get(position).getName());
        holder.Subtitulo.setText("#: " + listaFiltro.get(position).getId());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
            }
        });

        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        if (listaPokemon.size()==0 && listaFiltro.size()>0) listaPokemon.addAll(listaFiltro);
        if (listaPokemon.size()>0) {
            listaFiltro.clear();
            if (charText.length() == 0) {
                listaFiltro.addAll(listaPokemon);
            } else {
                for (Pokemon pok : listaPokemon) {
                    if (pok.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        listaFiltro.add(pok);
                    }
                }
            }
            notifyDataSetChanged();
        }
        Log.i("filtro Lista Pokemon", String.valueOf(listaPokemon.size()));
        Log.i("filtro Lista Pokemon 2", String.valueOf(listaFiltro.size()));
    }

}